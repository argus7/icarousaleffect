//
//  ViewController.swift
//  Icrousal
//
//  Created by Ravi Ranjan on 10/04/20.
//  Copyright © 2020 Ravi Ranjan. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,iCarouselDelegate , iCarouselDataSource {
    var items: [Int] = []
    
    @IBOutlet weak var icrousalView: iCarousel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.icrousalView.delegate = self
        self.icrousalView.dataSource  = self
        //Change with your style
        
        self.icrousalView.type = .rotary
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        for i in 0 ... 99 {
            items.append(i)
        }
    }
    
    public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let view = LoadNibClass.instanceFromNib()
        view.frame = CGRect(x: 0, y: 0, width: self.icrousalView.frame.width - 30, height: self.icrousalView.frame.width - 10)
        return view
    }
    
    public func numberOfItems(in carousel: iCarousel) -> Int {
        return items.count
    }
    
    
    
}
class LoadNibClass: UIView {
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CarousalView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
