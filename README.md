# ICarousalEffect

# STEPS to implement in your project



# 1. Clone this project
# 2. Get icarousal Folder and drag to your peoject
# 3. Add a view on your view controller with storyBoard or by code
# 4. set the class of view to iCarousel
# 5. create an IBOutlet for the view
# 6. create a XIB view for caraousal which you gonna reuse 
# 7. create a swift file of UIView class and set to XIB view
# 8. set delegate and data source to viewcontroller 

    @IBOutlet weak var icrousalView: iCarousel!
    self.icrousalView.delegate = self
    self.icrousalView.dataSource  = self
# 9. add delegate and datasource protocol to your view controller 
    class ViewController: UIViewController ,iCarouselDelegate , iCarouselDataSource {
    }
# 10. implement data source method : 
 
    public func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
     let view = LoadNibClass.instanceFromNib()
     view.frame = CGRect(x: 0, y: 0, width: self.icrousalView.frame.width - 30, height: self.icrousalView.frame.width - 10)
     return view
     }
 
    public func numberOfItems(in carousel: iCarousel) -> Int {
     return items.count
     }
# 11. create a class to load xib view :
    class LoadNibClass: UIView {
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CarousalView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        }
    }


